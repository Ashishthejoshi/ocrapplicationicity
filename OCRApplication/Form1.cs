﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Security.AccessControl;
using System.Threading;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using System.Threading;

namespace OCRApplication
{
    public partial class Form1 : Form
    {
        public string  FileExtensionPath="";
        public string LoadDirectoryPath = "";
        public string DirectoryPathForOCR = "";
        public Boolean OCRForDirectFolder = false;
        private static CountdownEvent AutoEvent = new CountdownEvent(3);

        System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
        private Thread currentThread;
        private SynchronizationContext Context;
        int ValueTowrite = 1;

        public Form1()
        {
            InitializeComponent();

            FileExtensionPath = Convert.ToString(ConfigurationSettings.AppSettings["FileExtensionPath"]);
            LoadDirectoryPath = Convert.ToString(ConfigurationSettings.AppSettings["LoadDirectoryPath"]);
            DirectoryPathForOCR = Convert.ToString(ConfigurationSettings.AppSettings["DirectoryPathForOCR"]);
            OCRForDirectFolder = Convert.ToBoolean(ConfigurationSettings.AppSettings["OCRForDirectFolder"]);
            if (OCRForDirectFolder)
            {
                LoadDirectoryPath = DirectoryPathForOCR;
            }

            try
            { 
                t.Interval = 10000;
                t.Tick += new EventHandler(Timer_Tick);
                t.Start();
                Context = SynchronizationContext.Current;
                Context = Context ?? new SynchronizationContext();
                 currentThread = new Thread(OCR);
                currentThread.Start();
 

            }
            catch (Exception ex)
            {
                CreateLogFileError("Form1 Load Error:" + ex);
            }

           


        }


        private void Timer_Tick(object sender, EventArgs e)
        {
            Context.Send(new SendOrPostCallback(s => OCR()), null);
        }


        Boolean CheckOcrWorking = false;
        protected void OCR()
        {
            if (CheckOcrWorking == false)
            {
                CheckOcrWorking = true;
                CreateLog("--OCR Started");
                GetStarttOCRFromIndexFolder();
                CreateLog("--OCR Completed");
                CheckOcrWorking = false;
            }
        }




        protected void GetStarttOCRFromIndexFolder()
        {
            string FolderNumber = "";
            try
            {
                  string[] filePathsFromIndex;
                if (OCRForDirectFolder)
                {
                    filePathsFromIndex = Directory.GetDirectories(LoadDirectoryPath);
                }
                else
                {
                    filePathsFromIndex = Directory.GetFiles(LoadDirectoryPath, "*." + FileExtensionPath.Trim() + "");
                }

                DateTime from_date = DateTime.Now.AddDays(-8);
                DateTime to_date = DateTime.Now.AddDays(2);
                
                
                var todayFiles = Directory.GetDirectories(LoadDirectoryPath)
                      .Where(x => new FileInfo(x).CreationTime.Date >= from_date.Date && new FileInfo(x).CreationTime.Date <= to_date.Date) 
                       .OrderByDescending(f => new FileInfo(f).CreationTime.Date);

 


                foreach (string SubFileFromIndex in todayFiles)
                {
                    string _regEx = @"\d{21}";
                    Regex Firstlinereg = new Regex(_regEx, RegexOptions.Compiled);
                    string _lines = string.Empty;
                    Match objMatch = Firstlinereg.Match(SubFileFromIndex);
                    if (objMatch.Success)
                    {

                          FolderNumber = objMatch.ToString().Trim();
                        if (File.Exists(LoadDirectoryPath + @"\" + FolderNumber + "\\OCRCompleted.txt") == false)
                        {
                            GetStartOCR(LoadDirectoryPath + @"\" + FolderNumber + "");
                           
                            CreateFileLogAfterOCRCompleteFolder(DirectoryPathForOCR + @"\" + FolderNumber, "Folder No :" + FolderNumber);
                         }
                        else
                        {
                            
                        }

                    }




                }
            }
            catch (Exception ex)
            {
                CreateLogFileError("GeStarttOCRFromINDEXF Error:"+ ex);

            }
        }


        


        protected void CheckOcrDoneFolder()
        {
            int OCRDoneFolder = 0;
            int OCRPendingFolder = 0;
            string FolderNumber = "";
            try
            {
                string[] filePathsFromIndex;
                if (OCRForDirectFolder)
                {
                    filePathsFromIndex = Directory.GetDirectories(LoadDirectoryPath);
                }
                else
                {
                    filePathsFromIndex = Directory.GetFiles(LoadDirectoryPath, "*." + FileExtensionPath.Trim() + "");
                }



                foreach (string SubFileFromIndex in filePathsFromIndex)
                {
                    string _regEx = @"\d{21}";
                    Regex Firstlinereg = new Regex(_regEx, RegexOptions.Compiled);
                    string _lines = string.Empty;
                    Match objMatch = Firstlinereg.Match(SubFileFromIndex);
                    if (objMatch.Success)
                    {
                        FolderNumber = objMatch.ToString().Trim();
                        if (File.Exists(LoadDirectoryPath + @"\" + FolderNumber + "\\OCRCompleted.txt") == false)
                        {

                            OCRPendingFolder = OCRPendingFolder + 1;
                           
                        }
                        else
                        {
                            OCRDoneFolder = OCRDoneFolder + 1;
                        }

                    }




                }
                

            }
            catch (Exception ex)
            {
                CreateLogFileError("CheckOcrDoneFolder Error:" + ex);

            }
        }


        protected void GetStartOCR(string BoxPathValue)
        {
            try
            {

                string BoxPath = BoxPathValue;
                string[] directoryListFolder = Directory.GetDirectories(BoxPath);

                foreach (string subdirectoryFolder in directoryListFolder)
                {
                    if (Directory.Exists(subdirectoryFolder))
                    {

                        string[] filesArray = subdirectoryFolder.Split(Path.AltDirectorySeparatorChar,
                                  Path.DirectorySeparatorChar);

                        if (File.Exists(subdirectoryFolder + "\\results" + filesArray[8] + ".csv") == false)
                        {
                            CreateLog("--OCR Started For FolderNo:-" + filesArray[7] + " and BoxNo:-" + filesArray[8] + "");
                                 OCRFolderWise(subdirectoryFolder, subdirectoryFolder);
                            CreateLog("--OCR Completed For FolderNo:-" + filesArray[7] + " and BoxNo:-" + filesArray[8] + "");
                        }
                        else
                        {

                        }

                    }
                }
              
            }
            catch (Exception ex)
            {
                CreateLogFileError("GetStartOCR Error:" + ex);

            }
           // }

               



        }


        private  void OCRFolderWise(string InPathValue, string outPathvalue)
        {

            try
            {
                
                string _pathExtract = InPathValue.Replace("i-CITY Sub012", @"\""i-CITY Sub012""\");
                string InPath = _pathExtract;
                string OutPath = outPathvalue.Replace("i-CITY Sub012", @"\""i-CITY Sub012""\"); ;
               

                Process _process = new Process();
                _process.StartInfo.FileName = "cmd.exe";
                _process.StartInfo.CreateNoWindow = true;
                _process.StartInfo.UseShellExecute = false;
                _process.StartInfo.RedirectStandardInput = true;
                _process.StartInfo.RedirectStandardOutput = true;
                _process.StartInfo.RedirectStandardError = true;
                _process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                _process.Start();
                var error = "";


           
                //var str = "\"cd /d C:\\Dario\\main_new\\main\"";
                string ExePath = Convert.ToString(ConfigurationSettings.AppSettings["ExePathForOCR"]).Replace("\\", "/");

                var str = "\"cd /d " + ExePath;
                _process.StandardInput.WriteLine("cmd /K " + str);
                _process.StandardInput.WriteLine(" main.exe --INPATH " + InPath + " " + "--OUTPATH " + OutPath);
                _process.StandardInput.Flush();
                _process.StandardInput.Close();
                _process.WaitForExit();
                Console.Write(_process.StandardOutput.ReadToEnd());
                error = _process.StandardError.ReadToEnd();
                GC.Collect();

                if (error != "")
                {
                    CreateLogFileError("InPath "+ InPathValue+ " OutPath "+ outPathvalue + " Error:" + error);
                }
            }
            catch (Exception ex)
            {
                CreateLogFileError("OCRFolderWise Error:" + ex);

            }
        }


       
        protected void CreateLog(string ValueTowrite)
        {
            string directory = AppDomain.CurrentDomain.BaseDirectory;
            string logdate = DateTime.Now.ToString("yyyyMMdd") ;
            string fileName = @""+ directory + "\\Log\\LogFileBoxOCRR_" + logdate + ".txt";
            try
            {
                // Check if file already exists. If yes, delete it.     

                if (File.Exists(fileName)==false)
                {
                    File.Create(fileName);
                }

                if (File.Exists(fileName))
                {
                    

                    using (StreamWriter w = File.AppendText(fileName))
                    {
                        string Value = DateTime.Now + " " + ValueTowrite;




                        if (txtOcrStatus.InvokeRequired)
                        {
                            txtOcrStatus.Invoke(new MethodInvoker(delegate
                            {
                                txtOcrStatus.Text = Value;
                            }));
                        }

                        //   txtOcrStatus.Text = Value;
                        w.WriteLine(Value.Trim());
                    }

                   
                }

            }
            catch (Exception Ex)
            {
                CreateLogFileError(Ex.ToString());
            }
        }

 

        public void CreateLogFileError(string ValueTowrite)
        {
            string directory = AppDomain.CurrentDomain.BaseDirectory;
            string logdate = DateTime.Now.ToString("yyyyMMdd");
            string fileName = @"" + directory + "\\Error\\ErrorLog" + logdate + ".txt";
            try
            {
                // Check if file already exists. If yes, delete it.     

                if (File.Exists(fileName) == false)
                {
                    File.Create(fileName);
                }

                if (File.Exists(fileName))
                {
                   

                    using (StreamWriter w = File.AppendText(fileName))
                    {
                        w.WriteLine(DateTime.Now + " " + ValueTowrite);
                    }


                }

            }
            catch (Exception Ex)
            {
                CreateLogFileError(Ex.ToString());
            }
        }


        protected void CreateFileLogAfterOCRCompleteFolder(string Path, string Value)
        {
            string directory = Path;
            string fileName = @"" + directory + "\\OCRCompleted.txt";
            try
            {
                // Check if file already exists. If yes, delete it.     

                if (File.Exists(fileName) == false)
                {
                    File.Create(fileName);
                }

                if (File.Exists(fileName))
                {
                     
                    using (StreamWriter w = File.AppendText(fileName))
                    {
                        w.WriteLine(DateTime.Now +" "+ "OCRCompleteFolder" + " " + Value);
                    }


                }

            }
            catch (Exception Ex)
            {
                CreateLogFileError(Ex.ToString());
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            

            try
            {
                Thread.Sleep(100);
                CreateLog("--OCR Started");
                GetStarttOCRFromIndexFolder();
                CreateLog("--OCR Completed");

                Thread.Sleep(100);

            }
            catch (Exception Ex)
            {
                CreateLogFileError(Ex.ToString());
            }
           
        }
       
     
    }
}
