﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{


    public partial class TimerCheck : Form
    {
        System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
        private Thread currentThread;
        private SynchronizationContext Context;
        int ValueTowrite = 1;

        public TimerCheck()
        {
            InitializeComponent();

            t.Interval = 10000;
            t.Tick += new EventHandler(Timer_Tick);
            t.Start();
            Context = SynchronizationContext.Current;
            Context = Context ?? new SynchronizationContext();
            currentThread = new Thread(new ThreadStart(CreateLog));
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            Context.Send(new SendOrPostCallback(s => CreateLog()),null);
        }

        public void CreateLog()
        {
            
            string directory = AppDomain.CurrentDomain.BaseDirectory;
            string logdate = DateTime.Now.ToString("yyyyMMdd");
            string fileName = @"" + directory + "\\Log\\LogFileBoxOCRR_" + logdate + ".txt";
            try
            {
                label1.Text = ValueTowrite.ToString();
                ValueTowrite++;

            }
            catch (Exception Ex)
            {
                
            }
        }


    }
}
